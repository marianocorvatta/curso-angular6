import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetaDeDestinoComponent } from './targeta-de-destino.component';

describe('TargetaDeDestinoComponent', () => {
  let component: TargetaDeDestinoComponent;
  let fixture: ComponentFixture<TargetaDeDestinoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetaDeDestinoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetaDeDestinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
